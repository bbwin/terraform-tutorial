# Terraform ref-card-03

This terraform script creates the required infrastructure for the Reference Card 03.

## Prerequisites
Install terraform from [official Hashicorp website](https://developer.hashicorp.com/terraform/downloads?product_intent=terraform).

[AWS CLI](https://aws.amazon.com/cli/) is required for AWS provider.

Copy credentials from (AWS Learner Lab)[https://awsacademy.instructure.com/] - "AWS Details" - "AWS CLI" and paste it into `~/.aws/credentials`

## Terraform

Steps to develop and run terraform:
- Initialize terraform once and download providers: `terraform init`
- Rewrite configuration files to canonical format: `terraform fmt`
- Validate terraform configuration: `terraform validate`
- Plan your execution: `terraform plan`
- Apply terraform configuration: `terraform apply`

## Testing

Terraform prints required outputs:
- database_endpoint
- database_port
- web_public_ip

Connect to EC2 instance: `ssh ubuntu@<web_public_ip>`

Install tools:
```sh
sudo apt install -y mariadb-client vim nginx
```

Connect to database
```sh
mariadb --host <database_endpoint> --port <database_port> --user admin --password
```

Get internal IP:
```sh
dig <database_endpoint>
```

Just test without mysql client:
```sh
nc -v <database_endpoint> --port <database_port>
```
