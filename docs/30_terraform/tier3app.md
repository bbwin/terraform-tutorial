# terraform - Jokes-DB

!!! abstract "Voraussetzungen"
    - [3-Tier-Architektur](../20_ansible/tier3app.md)
    - [JokesDB - Ref-Card-03](https://gitlab.com/bbwin/ref-card-03)

Unsere bisherige Applikation, die Jokes-DB soll nun mit *Terraform* umgesetzt werden. Wir verzichten dabei auf *CloudInit* und verwenden das AWS Learner Lab. Im Folgenden bauen wir ein umfangreiches *Terraform*-File auf und gehen Schritt für Schritt in die Details und erweitern die Komplexität. Viel Spass!

!!! task "Vorgehensweise"
    Es wird empfohlen der Anleitung konzentriert zu folgen und die Schritte für sich umzusetzen. Es wird alles genau beschrieben und jedes Zeichen müsste verstanden werden können. Nehmen Sie sich entsprechend Zeit!

## Zielsystem

Wir wollen hier die Ref-Card 03 aka Jokes-App in Betrieb nehmen. Das Zielsystem sieht wie folgt aus:

![Netzwerk-Schema](assets/overview.svg)

## Start

Unser `main.tf` sieht zu Beginn so aus.

```hcl
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "us-west-2"
}
```

## Virtual Private Cloud
AWS kennt das Konzept der [Virtual Private Cloud](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html), kurz VPC. Ein solches müssen wir erstellen, um darin arbeiten zu können. Wir aktivieren dabei dns Hostnames und definieren uns einen privaten IP-Adressblock.

```hcl
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
}
```

Mit dieser VPC wollen wir über einen [Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html) kommunizieren können:
```hcl
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}
```
Wir haben diesen Gateway direkt ans oben definierte VPC angehängt, indem wir die `vpc_id` von der Ressource `aws_vpc` übernehmen.

## Subnetze

### Öffentliches Subnetz
Zuerst wollen wir uns ein öffentliches Subnetz definieren. In dieses Subnetz wird unsere Applikation gesetzt. Dieses Subnetz wird unserem VPC angefügt und ein IP-Sub-Bereich gewählt. Der IP-Bereich entspricht einer internen Adressierung. Dank `map_public_ip_on_launch` werden Instanzen in diesem Subnetz öffentliche IP-Adressen zugeordnet.
```hcl
resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true
}
```

### Private Subnetze
Etwas komplizierter verhält es sich beim internen Subnetz. Hier werden wir für den RDS (Relational Database Service) zwei interne Subnetze benötigen. Um ein bisschen in die Macht von *Terraform* einzutauchen, zeigen wir, wie hier Automatismen die Lösung schlanker machen.

```hcl
resource "aws_subnet" "private" {
  count = 2
  vpc_id = aws_vpc.main.id
  cidr_block = cidrsubnet(aws_vpc.main.cidr_block, 8, 10 + count.index)
}
```

Dank `count = 2` werden gerade zwei private Netze erstellt. Natürlich werden diese dem obigen VPC zugeordnet.

[`cidr_block`](https://developer.hashicorp.com/terraform/language/functions/cidrsubnet): Als IP-Adressierung wird der CIDR-Block vom VPC genommen und das Prefix um 8 erhöht (vorher `/16`, jetzt `/24`). Zudem wird der erhöhte Block um das dritte Argument erhöht. Und da `count.index` *0* bzw. *1* entspricht, ergibt sich im dritten Oktett *10* und *11*. Lange Rede kurzer Sinn: Die CIDR-Blöcke der Private-Subnetze lauten `10.0.10.0/24` und `10.0.11.0/24`.

### Availability Zones
Für mehr Ausfallsicherheit wäre es für die RDS wichtig, dass die beiden Subnetze jeweils in verschiedenen *Availability Zones* platziert werden. Dazu müssen wir ein bisschen ausholen. 

Als erstes holen wir uns dank einer `data`-Ressource die verfügbaren Availability Zones:
```hcl
data "aws_availability_zones" "available" {
  state = "available"
}
```
Auf diese Daten können wir dann innerhalb der `aws_subnet` Ressource `private` zurück greifen. Wieder verwenden wir den `count.index` zur Unterscheidung der beiden Subnetze:
```hcl
  availability_zone = data.aws_availability_zones.available.names[count.index]
```
### Tags
Und zu guter Letzt wollen wir unsere Netze noch mit einem Ettikett (*Tag*) versehen, damit wir sie später referenzieren können.
```hcl
  tags = {
    Name = "private_${data.aws_availability_zones.available.names[count.index]}"
  }
```

Konnten Sie bis hierher folgen? **TODO**: Kontrolle

## Routing
Auch in Clouds muss/darf/soll geroutet werden. Wir erstellen erst eine Routing-Tabelle für das Public Netzwerk und lassen jeglichen Verkehr zu. Dieser wird über das [Gateway](#virtual-private-cloud) geführt.
```hcl
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}
resource "aws_route_table_association" "public" {
  route_table_id = aws_route_table.public.id
  subnet_id = aws_subnet.public.id
}
```
Für das private Netzwerk lassen wir kein Routing zu.
```hcl
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
}
resource "aws_route_table_association" "private" {
  count = length(aws_subnet.private)
  route_table_id = aws_route_table.private.id
  subnet_id = aws_subnet.private[count.index].id
}
```

## Security Group
AWS kennt das Konzept der *Security Group*. Damit wird definiert, was erlaubt ist. Sicherheit basiert auf dem Grundsatz, dass nichts erlaubt ist, was nicht explizit erwähnt ist. Verallgemeinerungen sind schlecht, besser ist das granulare Vorgehen.
### EC2
Wir erstellen deshalb erst eine Security Group für die EC2-Instanzen der Webserver. Deshalb wählen wir den Namen entsprechend.

Der Start ist wohl mittlerweile selbsterklärend:
```hcl
resource "aws_security_group" "ec2" {
  name        = "ec2-allow-ssh-http"
  description = "Security group for the EC2 instance"
  vpc_id      = aws_vpc.main.id
}
```

Natürlich folgen noch ein paar Regeln in diese Sicherheitsgruppe. Es werden Regeln für `ingress` (Eingang) und `egress` (Ausgang) unterschieden. Man kann dabei gerade auch ein mapping durchführen. Also was von einer Seite zur Firewall ankommt, muss nicht zwingend auch genau so weiter geleitet werden.

Als erstes bauen wir eine `ingress`-Regel ein, welche uns Zugriff auf SSH (tcp-Port 22) erlaubt:
```hcl
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
```
!!! task "`egress`-Regel all allowed"
    Bauen Sie eine `egress`-Regel dazu, welche jeglichen Traffic zulässt. Als Protocol wird dazu `-1` gewählt.

!!! task "ingress tcp 80 -> tcp 8080"
    Die Jokes-Applikation wird auf Port 8080 hoch gefahren. Das gefällt uns natürlich nur beschränkt, wir wollen sie auf dem regulären HTTP-Port 80 dem Benutzer verfügbar machen.

    Bauen Sie deshalb eine `ingress`-Regel ein, welche auf tcp-Port 80 Inhalte entgegennimmt, diese aber auf tcp-Port 8080 leitet.

Und jetzt kommt etwas dazu, was nichts mit AWS, sondern eine *Terraform*-Problematik löst. Wenn *Terraform* ausgeführt wird, baut es bei Änderungen unnötigerweise Infrastruktur neu auf. Wenn nun aber z.B. die Security Group gelöscht wird (um sie in abgeänderter Form wieder aufzubauen), löst dies eine Kettenreaktion in AWS aus. Es werden automatisch Ressourcen entfernt, welche dieser Security Group zugeordnet sind. Dies betrifft insbesondere die EC2-Maschinen, die wir später daran anhängen.

Um dieses Phänomen zu vermeiden, wird folgende Einstellung eingefügt.

```hcl
  lifecycle {
      create_before_destroy = true
  }
```
### RDS
Ganz ähnlich benötigen wir eine Security Group für den RDS:
```hcl
resource "aws_security_group" "rds" {
  name        = "rds-allow-sql"
  description = "Security group for the RDS instance"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.ec2.id]
  }
  lifecycle {
      create_before_destroy = true
  }
}
```

Speziell ist hier nur die Einstellung `security_groups`. Diese besagt, dass nur Traffic von Instanzen erlaubt ist, die der oben definierten Security Group zugeordnet sind. Wie hübsch ist das denn? Wir brauchen uns hier um nichts zu kümmern und können einfach auf andere Elemente unserer *Terraform*-Infrastruktur zugreifen. Wirklich hübsch!

## RDS-Instanzen
Endlich können wir uns mal um etwas kümmern, was wir eigentlich haben wollten. Nämlich eine Datenbank. Wir wählen Maria-DB:
```hcl
resource "aws_db_instance" "mariadb" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mariadb"
  engine_version         = "10.6.14"
  instance_class         = "db.t2.micro"
  db_name                = "mariadb"
  username               = "admin"
  password               = "mypassword"
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds.id]
  db_subnet_group_name   = aws_db_subnet_group.private.name
}
```

Die meisten Einstellungen sind wohl nachvollziehbar. Wir wollen mit `skip_final_snapshot` die benötigten Ressourcen klein halten. Wir sind ja im Labor-Betrieb, da dürfen wir ein paar Komplexitäten weglassen.

Wichtig sind die Verbindungen zur oben definierten Security Group und welche Subnetz-Gruppe wir verwenden. Aha, die zwei privaten Subnetze sind in einer gemeinsamen Gruppe zu finden.

Ups, die müssen wir ja erst definieren:
```hcl
resource "aws_db_subnet_group" "private" {
  name = "db_subnet_group"
  subnet_ids = [for subnet in aws_subnet.private : subnet.id]
}
```
!!! task "for-Schleife?"
    Ist das jetzt wirklich eine for-Schleife bei `subnet_ids`? Gibt womöglich das [*Terraform*-Manual](https://developer.hashicorp.com/terraform/language/expressions/for) Auskunft?

## EC2-Webapplikation
Jetzt haben wir das Backend und brauchen noch den Webserver. Das ist leider nicht ganz so einfach, weil wir uns um möglichst nichts kümmern wollen.

Wir starten mit folgendem Code, welcher noch ganz simpel ist.
```hcl
resource "aws_instance" "ec2_instance" {
  instance_type   = "t2.micro"
}
```
Es fehlt das `ami`. Das ist das Basis-Abbild, auf welchem die Instanz hochgefahren wird. Man würde dies mit `ami=...` angeben. Dafür müsste man dann wohl in die AWS Console (WebGUI) und die ID dieses ami copy/pasten. Das geht natürlich. Wir wollen aber den dynamischen Weg mit *Terraform* gehen und uns die Auswahl des `ami` dank eines Filters auslesen lassen.

Wir erstellen eine `data`-Ressource, welche das aktuellste AMI ausliest (`most_recent`). Wir schränken dank je einem Filter den Namen und auch den Virtualisierungs-Typ ein. Zudem wollen wir nur ein von AWS offiziell verfügbares Image. (Sowas lässt sich später einfach copy/pasten)
```hcl
data "aws_ami" "ubuntu_22_04" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}
```
Dank dieser `data`-Ressource können wir bei der EC2-Instanz einfach das ami referenzieren:
```hcl
  ami = data.aws_ami.ubuntu_22_04.id
```
Weiter wollen wir die EC2-Instanz ins Public-Netzwerk platzieren und sie unserer extra dafür erstellten Security Group zuordnen.
```hcl
  subnet_id = aws_subnet.public.id
  vpc_security_group_ids = [aws_security_group.ec2.id]
```
Wir kennen auch schon das Problem mit dem lifecycle von *Terraform*. Wir wollen hier speziell keine neue Maschine erstellen lassen, falls sich das AMI aktualisiert. Wir wählten das neueste AMI, wenn also beim späteren Durchlauf ein noch neueres AMI existiert, soll uns das egal sein. Es lohnt sich dann nicht, die Maschine zu ersetzen.
```hcl
  lifecycle {
    ignore_changes = ["ami"]
  }
```
Beim MaaS mussten wir für den Zugang per SSH einen Public Key per CloudInit mitgeben. Das ist bei AWS nicht anders.
Wir müssen dazu eine Ressource eines Schlüsselpaars erstellen:
```hcl
resource "aws_key_pair" "rsa" {
  key_name   = "ssh_key"
  public_key = file("~/.ssh/id_rsa.pub")
}
```
... und können diese dann der aws_instance Ressource zuweisen:
```
  key_name = aws_key_pair.rsa.key_name
```
Das war jetzt auch nicht ganz so simpel. Halten Sie inne und überprüfen Sie das Ergebnis!

## Outputs
Das war eine grössere Übung! Und viele Fallstricke sind enthalten. Tatsächlich wäre vieles simpler gegangen, dafür aber statisch und deshalb nicht so elegant. Vor allem aber lernten Sie sehr viele Elemente und auch etwas die Denkweise von *Terraform* kennen.

Als Ergebnis haben Sie natürlich die Infrastruktur im Learner Lab. Ein paar Elemente wären jetzt aber spannend, um überhaupt auf die Infrastruktur zugreifen zu können.

Wir zeigen als Output die Public IP der EC2-Instanz, die Adresse der Maria-DB und den Port der Maria-DB an.
```hcl
output "web_public_ip" {
  description = "The public IP address of the web server"
  value       = aws_instance.ec2_instance.public_ip
}
output "database_endpoint" {
  description = "The endpoint of the database"
  value       = aws_db_instance.mariadb.address
}
output "database_port" {
  description = "The port of the database"
  value       = aws_db_instance.mariadb.port
}
```

## Wie geht's weiter
Tatsächlich haben wir mit diesem riesigen *Terraform*-Konstrukt nur die Plattform hochgefahren. Es ist weder eine Applikation darauf lauffähig, noch ist das RDBMS mit einer Datenbank ausgestattet.

Diese Arbeiten lösen wir nun mit *Ansible*. Da Sie das entsprechende Kapitel bereits durchspielten, gehen wir davon aus, dass Sie das hinkriegen sollten. Sie haben dies ja bereits auf dem *MaaS* umgesetzt, da ist vieles zu adaptieren.

!!! task "Modularer Aufbau"
    Überlegen Sie sich, wie Sie dieses umfangreiche main.tf in mehrere Dateien aufteilen. *Terraform* fügt automatisch alle Dateien im Verzeichnis zusammen und die Reihenfolge der Ressourcen spielt keine Rolle. Eine sehr detailierte und sinnvolle Best-Practice-Anleitung finden sie in den [Google Cloud Docs](https://cloud.google.com/docs/terraform/best-practices-for-terraform#subdirectories). Die erwähnten Beispiele sind auf komplexe Produktionsumgebungen zugeschnitten, verwenden Sie nur die Teile welche für Sie und Ihre Projekt Sinn ergeben.

    Wenn Sie das Projekt für sich sauber ordnen, finden Sie die entsprechenden Passagen wieder und können in Zukunft viele Elemente wieder verwenden.

!!! success "Lernerwartung"
    Sie haben nun mittels *Terraform* und *Ansible* eine vollständige Anwendung nach der 3-Tier-Architektur vollautomatisch hoch gefahren.