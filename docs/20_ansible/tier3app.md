# Ansible: Anwendung

!!! abstract "Voraussetzung"
    - Zugang zum [MaaS](../10_cloudinit/index.md)
    - Lektüre [Ansible Einstieg](index.md)
    - Übung zu [Statische Website im MaaS](../10_cloudinit/static-website.md)

!!! info "Ref Card 03 Jokes DB"
    Als Anwendung verwenden wir die Jokes Datenbank [Ref Card 03](https://gitlab.com/bbwin/ref-card-03).

## 3-Tier-Architektur

Ein wesentliches Konzept zur Applikationsaufteilung ist die *3-Tier Architektur*. Im Grundsatz trennt man dabei Frontend, Backend und Storage bzw. Darstellung, Logik und Daten.

![3 Tier Architecture](https://ucarecdn.com/83a9f601-0632-4fe9-b05f-185bb192235e/)

Meist wird das Frontend beim Client angezeigt und in Form einer Website, einer Desktop-Applikation oder einer Mobile-App umgesetzt. Im folgenden geht es eher um die Serverseite. Hier benötigen wir oft einen Datenbankserver und einen Applikationsserver.


## Jokes DB (Ref Card 03)

1. Netzwerkschema: Skizzieren Sie die Applikation kurz auf. Zeichnen Sie ein Netzwerkschema!

    Wir trennen Provisionierung und Deployment der Maschinen. Das hat zur Folge, dass wir die VMs sehr generalistisch aufsetzen können und das Individuelle anschliessend `ansible` überlassen. Da `ansible` einfach einen SSH-Zugang benötigt, werden wir per `CloudInit` genau das und nichts zusätzliches möglich machen.

1. Back to the roots: Erstellen Sie einen User für `ansible` und erstellen Sie zwei VMs dieser Art.

1. Erstellen Sie nun `hosts`, `ansible.cfg` und Ihre Playbooks. Das letztere bedeutet, dass Sie verstehen müssen, wie die Applikation in Betrieb genommen werden muss. Da lohnt es sich, dies zuerst manuell auszuprobieren.

!!! task "Rückblende"
    Ihre Applikation läuft? Aber haben Sie auch verstanden, was Sie alles dafür taten? Zeichnen Sie die Aktion grafisch auf!

!!! success "Lernerwartung"
    Sie haben gelernt, wie ...

    - `CloudInit` und `ansible` zusammen genutzt werden
    - mit `ansible` mehrere Systeme konfiguriert werden

    Diese Kompetenzen werden Sie im weiteren benötigen.