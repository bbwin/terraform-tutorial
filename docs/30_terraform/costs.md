# Kostenberechnung mit Infracost

!!! abstract "Voraussetzungen"
    - terraform für 3-Tier-Applikation JokesDB

!!! abstract "Lernziele"
    HZ 2: Spezifiziert die Kosten einschliesslich Betriebsaufwand der vorgeschlagenen Lösung und bestimmt die zweckmässige Cloud Adoption.

## Kostenberechnung

Eine einfache Schätzung der zu erwartetenden Kosten kann mit dem Tool [*Infracost*](https://www.infracost.io/) realisiert werden. *Infracost* basiert direkt auf den *Terraform* Beschreibungen und kennt die Kosten der HyperScaler. Mit bestimmten Parametern können verschiedene Szenarien berechnet werden.

## Praxis

Lernen ist aktives Handeln: Ausprobieren ist angesagt! *Infracost* bietet für individuelle Entwickler kostenlose Dienste an. Diese reichen uns für unsere Experimente. Als Erstes müssen Sie sich bei *Infracost* anmelden. Sie benötigen dazu eine E-Mail-Adresse (z.B. diejenige der *BBW*). Das Registrierverfahren mit Ihrem Code Repository können Sie auch gut auf später verschieben. Wir benötigen diese Funktionalität (noch) nicht.

Nach der Registrierung finden Sie im Dashboard im Titel die *Org Settings*. Hier finden Sie den *API-Key*, den Sie später benötigen werden. Ebenfalls finden Sie da einen Link auf die infracost CLI bzw. die Installationsanleitung davon. Diese enthält auch gerade ein How-To, wie Sie vorgehen müssen.

!!! task "Kosten der Jokes-App"
    Nutzen Sie den Dienst und errechnen Sie die Kosten der Jokes-App, wie wir im letzten Kapitel bauten.

!!! task "Optimieren"
    Finden Sie geeignete Mittel, um die Kosten bei *AWS* zu senken. Wählen Sie dazu verschiedene Anforderungen bzw. Betriebsvarianten, und zeigen Sie auf, welche Anforderungen zu welchen Kostentreibern führen.