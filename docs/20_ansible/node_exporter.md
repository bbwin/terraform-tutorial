# Applikation einrichten

## Aufgabenstellung
!!! info "Prometheus / Grafana"
    Es existiert das Framework [Prometheus / Grafana](https://prometheus.io/docs/visualization/grafana/), welches für Monitoring benutzt wird. Wir interessieren uns in dieser Übung nur für den *node_exporter*. Das ist ein Stück Software, welches auf eine zu überwachenden Maschine läuft und auf einem bestimmten Netzwerk-Port Zugang zu den verschiedenen Messwerten der Maschine ermöglicht. Prometheus geht dann regelmässig diesen *node_exporter* anfragen, um die gewünschten Werte in seine time-series-Datenbank zu erfassen.

Die Idee der Übung: Erstellen Sie anhand einiger Hinweise das fertige Ansible-Projekt. Aus didaktischen Gründen wählen wir den harten Weg: alles selbst machen. Dies soll alles per Ansible erledigt werden:

1. [Download-Website von prometheus](https://prometheus.io/download/#node_exporter) vom node_exporter als tar.gz
1. Entpacken des tar.gz
1. Kopieren Sie die entsprechende Datei nach `/usr/local/bin`.
1. Richten Sie ein [systemd-Unit-File](https://wiki.ubuntuusers.de/systemd/Units/) ein, welches den Dienst beim boot startet und überwacht.
1. Aktivieren Sie den Dienst
1. Starten Sie den Dienst

Testen Sie das Ergebnis:

- Läuft der Prozess
- Ist der TCP-Port geöffnet (listening)
- Und wenn Sie den Prozess killen?
- Und nach einem Reboot?

!!! task "Lernprodukt"
    Erstellen Sie ein README.md zu Ihrem Projekt, welches ordnet und zusammenfasst, was Sie lernten. Vergessen Sie die Reflexion nicht und fügen Sie Ihre Lehrperson als Guest hinzu.

!!! success "Lernerwartung"
    Sie haben hier anhand einer Beschreibung eine eigene Ansible-Lösung entwickelt.