# Statische Website mit CloudInit

!!! abstract "Voraussetzung"
    - [Einstieg ins MaaS](index.md)

!!! goal "Ziel"
    Mittels *Cloud-Init* erstellen wir eine neue VM inkl. fixfertiger Website. Ausser dem *Cloud-Init* brauchen wir keine weiteren Zutaten, insbesondere keinen manuellen Eingriff bis die statische Website angezeigt werden kann.

!!! info "Anmerkung"
    Dieses Vorgehen ist nicht unbedingt best practice, sondern ist aus didaktischen Gründen so gewählt.

## Manuelle Installation

Bevor wir die Situation automatisieren, lohnt es sich eigentlich immer, sie erst manuell auszuführen und den Vorgang kennen zu lernen. Dabei könnten wir auch auf Probleme stossen, die immer besonders wichtig zu beachten sind, weil sie bei der Automation uns beachtliche Hürden bedeuten könnten.

Auftrag: Starten Sie gemäss [Einstieg ins MaaS](index.md) eine VM auf dem MaaS, installieren Sie nginx und richten Sie eine kleine Website (`index.html` reicht) ein. Diese könnte z.B. wie folgt aussehen. Abtippen oder copy/paste per `cat` in einer SSH-Session ist erlaubt.

```ìndex.html
<!DOCTYPE html>
<html>
 <head><title>Meine kleine Beispielsseite</title></head>
 <body>
  <h1>Meine Beispielsseite</h1>
  <p>Diese bereitet mir Freude!</p>
 </body>
</html>
```

Notieren Sie sich die Schritte, die automatisiert werden müssen.

## Ausgangslage

Ausgangslage ist eine VM, auf welche wir (zu debugging-Zwecke) einen User einrichten. Also ganz so, wie es im Einstieg / im Film gezeigt wird.

## Paketinstallation

Cloud-Init bietet das Modul [packages](https://cloudinit.readthedocs.io/en/latest/reference/examples.html#install-arbitrary-packages), um mit dem jeweiligen Paketverwalter ein entsprechendes Paket zu installieren. In unserem Fall wollen wir `nginx` installieren.

Als Sie das Paket manuell installierten, mussten Sie hoffentlich erst `apt-get update` ausführen. Damit aktualisiert man das lokale Verzeichnis (repository) aller aktuell verfügbaren Softwarepaketen inkl. derer verfügbaren Version. Wenn das repository veraltet ist, versucht Ihre Instanz das Paket (*.deb) von einem veralteten und womöglich nicht mehr existierenden Link herunter zu laden. Deshalb ist es wichtig, immer vor einer Installation das repository zu aktualisieren.

Oft liest man im Internet, dass auch `apt-get upgrade` ausgeführt werden soll. Damit werden alle installierten Pakete aktualisiert. Das kann man schon machen, kostet aber unnötig Zeit. Installieren Sie ein neues Paket, welches auf einer neueren Bibliothek (Library) basiert, wird diese automatisch mit aktualisiert. Der Rest der Installation darf eigentlich auch eine Weile alt sein, ohne dass unser Testsystem gerade nicht mehr funktioniert. Deshalb empfehlen wir Ihnen auf das Upgrade zu verzichten. Sie sind schlichtweg einfach schneller fertig und haben keinen Nachteil daraus.

Konkret bauen wir also einen Abschnitt zur Paketinstallation von nginx ein:
```yaml
package_update: true
package_upgrade: false
packages:
  - nginx
```

CloudInit lässt sich schlecht debuggen, weshalb es sich lohnt in kleinen Schritten zu erweitern. Probieren Sie deshalb diesen Zwischenschritt aus und schauen Sie auf der VM, ob nginx tatsächlich installiert wurde und läuft (Default-Testseite sollte erscheinen).

## Statische Website

Wir wollen nun auch eine obige eigene `index.html` einbauen.

Dazu stellt CloudInit das Modul `write_files` zur Verfügung. Sie finden Beispiele auf [ReadTheDocs](https://cloudinit.readthedocs.io/en/latest/reference/examples.html#writing-out-arbitrary-files). Achten Sie auf die Einrückung des Texts. Nutzen Sie die Attribute `permissions` und `path`.

CloudInits sollten klein bleiben, damit sie wartbar bleiben. Spannend ist die Möglichkeit, den Inhalt mittels `gzip` zu komprimieren und per `base64` in eine lesbare Zeichendarstellung zu codieren. Sie kennen diese Verfahren hoffentlich aus Modul 114. Benutzen Sie das Verfahren und setzen Sie das Attribut `encoding` auf `base64+gzip`. Hilfe finden Sie in der [Referenz](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#write-files) unter Config Schema.

<details>
<summary>Idealerweise versuchen Sie es selbst.</summary>
Mein <a href="https://www.youtube.com/watch?v=Zu2sGJH2ZRs">YouTube-Video</a> könnte Ihnen helfen, falls Sie nicht weiter kommen.
</details>

## Statische Website mit Bild

Ergänzen Sie nun wie im Video auch angemerkt die Aufgabe, ein statisches Bild in die Website einzubauen. Das Bild soll ebenfalls auf dem Webserver liegen und also mit einem lokalen Pfad referenziert werden.

## Ergebnis

Mittels eines *CloudInit-YaML-File* erstellen Sie eine VM mit laufender Applikation. Diese benötigt keinerlei manuelle Eingriffe, um fertig zu sein. Ein Applikations-Lernender fragte mich einst, bevor wir mit dieser Übung starteten, ob man das nicht generieren lassen könne. Gehen Sie das *CloudInit* nochmals durch: Gibt es etwas, was hätte generiert werden müssen? Die Datei ist äusserst schlank und beschreibt den Endzustand. Das *Wie* wird nicht programmiert.

!!! success "Lernerwartung"
    Sie haben in dieser Session gelernt, ...

    - Pakete mit *CloudInit* zu installieren
    - Dateien mit *CloudInit* zu schreiben
    - eine vollständige Installation vollautomatisiert mit einem *CloudInit* vorzunehmen

    Diese Kompetenzen benötigen Sie im Folgenden.