# IaC - Infrastructure as Code

Was ist die *Cloud*? Was verstehen Sie unter dem Begriff *Cloud*?

Viele meinen damit die Benutzung von Rechenleistung oder Speicherplatz bei einem Hyperscaler. Doch wenn die Frage aufkommt, was denn *Cloud* neues bringt, können wir eigentlich alle typischerweise aufgezählten Elemente der Cloud als alt bezeichnen. Verteiltes Rechnen existierte bereits mit den UNIX-Rechnern in den 1970er, auch Virtualisierung beherrschten IBM-Mainframes in dieser Zeit bereits. Zentralisierte Storage-Systeme sowieso. Und global genutzte Infrastrukturen kennt man in global tätigen Unternehmen längst.

Was wirklich neu ist, ist IaC! Die Möglichkeit die Infrastruktur vollständig in Code zu beschreiben und diese hoch zu fahren, verändert die ICT grundlegend. Es wird die Basis sein aller zukünftigen Infrastrukturen, weil dies so viele Vorteile bietet.

In diesem Tutorial werden drei mögliche Sprachen vorgestellt und erarbeitet, die im Zusammenspiel die Mächtigkeit besitzen, riesige Infrastrukturen auszustatten. Wir erachten sie als "must known* - muss man sich antun!

- [CloudInit](10_cloudinit/index.md) wurde von Canonical entwickelt und wird vor allem im Ubuntu-Umfeld stark gepusht.
- [Ansible](20_ansible/python.md) wurde von RedHat entwickelt und bringt als bisher einzige Möglichkeit, deskriptiv aber ohne fixen Host Infrastrukturen zu verändern.
- [Terraform](30_terraform/index.md) wurde von HashiCorp entwickelt und bietet ein Mittel, um MultiCloud-Lösungen plattform- und vendor-unabhängig zu beschreiben.

Wir wünschen Ihnen viel Spass und Erfolg beim Einstieg in diese herausfordernde aber spannende Welt!