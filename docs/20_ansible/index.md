# Ansible Einstieg

!!! abstract "Voraussetzungen"
    - [Visual Studio Code](https://code.visualstudio.com/download)
    - [Remote Development extension pack](https://aka.ms/vscode-remote/download/extension)
    - Windows: Ubuntu WSL

## Einordnung

CloudConfig zeigt sich ziemlich unangenehm, wenn manchmal ein paar Modulabhängigkeiten nicht so funktionieren, wie gewünscht. Auch kann man mit CloudConfig keine bestehenden Infrastrukturen ändern.

!!! info "Begriffserklärung"
    *Ansible* ist ein Open-Source-Automatisierungswerkzeug zur Orchestrierung und allgemeinen Konfiguration und Administration von Computern. Es kombiniert Softwareverteilung, Ad-hoc-Kommando-Ausführung und Software-Configuration-Management. Die Verwaltung von Netzwerkcomputern erfolgt unter anderem über SSH und erfordert keinerlei zusätzliche Software auf dem verwalteten System. Module nutzen zur Ausgabe JSON und können in jeder beliebigen Programmiersprache geschrieben sein. Das System nutzt YAML zur Formulierung wiederverwendbarer Beschreibungen von Systemen. [Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Ansible)]

!!! task Auftrag
    Die Zusammenfassung von WikiPedia ist derart dicht, dass Sie jeden Satz separat mit Ihrem Tandem besprechen müssen. Einzelne Wörter sind wohl noch unbekannt. Versuchen Sie diesen trotzdem einen Inhalt zu geben und diskutieren Sie eine mögliche Lösung. Welche Fragen bleiben übrig? Formulieren Sie diese und stellen Sie sie der Lehrperson.

## Inbetriebnahme

Sie benötigen das Python-Modul *ansible* (und später weitere). Erstellen Sie für diese Übung ein neues Arbeitsverzeichnis, setzen `ansible` in die `requirements.txt` und richten sich ein venv ein:
```
python3 -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
```

## Ansible Configuration
Ansible erwartet im aktuellen Verzeichnis die Datei `ansible.cfg`. Wir nehmen darin zwei Einstellungen. Die zweite ist die offensichtlichere: Sie verweise darin auf Ihr Host-Inventar. Wir wählen dazu die Datei `hosts`, die wir unten erstellen werden.

Andererseits werden Sie im MaaS (und allgemein in Labor- und Testumgebungen) die Erfahrung machen, dass dieselben IP-Adressen für neue Systeme wieder verwendet werden. Konkret erstellen Sie z.B. eine VM mit der IP 192.168.67.23, arbeiten damit und verwerfen diese später wieder. SSH merkt sich beim Zugriff aber den Fingerprint, um merken zu können, falls Sie einen anderen Host untergejubelt kriegen. Wenn Sie später eine neue VM (mit neuem Host-Schlüssel) mit derselben IP-Adresse erstellen und darauf zugreifen wollen, wird Sie SSH daran hindern versuchen. Es könnte nämlich sein, dass Sie auf ein fremdes System geleitet wurden und jetzt die Logindaten abgefragt würden. In unserem Szenario möchten wir aber explizit auf dieses Sicherheitsverfahren verzichten, weshalb wir das `host_key_checking` ausschalten.

```ini
[defaults]
host_key_checking = no
inventory = hosts
```

## Inventory
Jetzt kümmern wir uns um das Inventory. Auch hier greift Ansible auf das INI-Dateiformat zurück. So können die Hosts auch gruppiert werden. Hier ein Beispiel eines solchen Inventar. Sie müssen diese Datei natürlich komplett anders verfassen. Wir nennen die Datei wie oben im `ansible.cfg` deklariert `hosts`:

```ini
[web]
web1 ansible_host=192.168.1.100 ansible_user=ubuntu
web2 ansible_host=192.168.1.101 ansible_user=ubuntu

[proxy]
rpx1 ansible_host=192.168.1.100 ansible_user=root
```

!!! info "SSH-Key"
    Standardmässig wird der SSH-Key unter `$HOME/.ssh/id_rsa` erwartet.  Alternativ kann neben `ansible_host` und `ansible_user` zusätzlich `ansible_ssh_private_key_file` definiert werden.

Test, ob das Inventory korrekt ist und Ansible über `pip` erfolgreich installiert wurde:

```sh
$ ansible-inventory --graph
@all:
  |--@ungrouped:
  |--@web:
  |  |--web1
  |  |--web2
  |--@proxy:
  |  |--rpx1
```

Test, ob die Verbindung funktioniert: ([Manual zum ping-Modul](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html))

```sh
$ ansible all -m ping
web1 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

Im Erfolgsfall wissen Sie, dass Ansible sich auf die Hosts verbinden kann und diese Hosts jetzt für Ansible erreichbar sind.

## Playbook
Letzte Komponente ist das playbook, welches beliebig benannt sein kann. Hier verwenden wir `site.yml`. Mittels `web` wird die gleichnamige Inventory-Gruppe angesprochen, alternativ können z.B. mit `all` alle Hosts im Inventory angesprochen werden.

Im vorliegenden Beispiel installieren wir einfach nur *nginx*.

```yml
---
- hosts: web
  gather_facts: yes
  become: yes

  tasks:
    - name: Install basic packages
      ansible.builtin.package:
        name:
          - nginx
```

Ansible im check-mode ausführen (Verbindet auf Nodes, führt aber keine Änderungen durch / dry-run):

```sh
$ ansible-playbook site.yml -C
```

Dies kann auf gewisse Hosts oder Gruppen beschränkt werden (`web` für Gruppe, `web1` für Host)
```sh
$ ansible-playbook site.yml -C -l web1
```

Do the real thing:
```sh
$ ansible-playbook site.yml -l web1
```

!!! success "Lernerwartung"
    Aus dieser Einheit nehmen Sie mit:

    - das Konzept von Ansible
    - die Elemente Config, Inventar und Playbook
    - wie man Ansible ausführt.