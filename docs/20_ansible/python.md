# Python / venv / ansible

Im ersten Lehrjahr lernten Sie als Plattformer Python im M319, jedoch nicht die Applikationsentwickler. Diese Seite richtet sich an jene, die also die Grundlagen von Python-Installationen noch nicht hatten bzw. vergassen.

## Installationsmethoden

!!! info "Begriffserklärung"
    Python ([ˈpʰaɪθn̩], [ˈpʰaɪθɑn], auf Deutsch auch [ˈpʰyːtɔn]) ist eine universelle, üblicherweise interpretierte, höhere Programmiersprache. Sie hat den Anspruch, einen gut lesbaren, knappen Programmierstil zu fördern. So werden beispielsweise Blöcke nicht durch geschweifte Klammern, sondern durch Einrückungen strukturiert.

    Python unterstützt mehrere Programmierparadigmen, z. B. die objektorientierte, die aspektorientierte und die funktionale Programmierung. Ferner bietet es eine dynamische Typisierung. Wie viele dynamische Sprachen wird Python oft als Skriptsprache genutzt. Die Sprache weist ein offenes, gemeinschaftsbasiertes Entwicklungsmodell auf, das durch die gemeinnützige Python Software Foundation gestützt wird, die die Definition der Sprache in der Referenzumsetzung CPython pflegt. [Quelle: [WikiPedia](https://de.wikipedia.org/wiki/Python_(Programmiersprache))]

Sie werden Python für Ihren Beruf dauernd benötigen. Insbesondere wenn Sie mit Ansible arbeiten, weil Ansible in Python entwickelt wird. Deshalb installieren Sie Python auf Ihrem persönlichen Arbeitsgerät. Dazu stehen Ihnen verschiedene Methoden zur Verfügung:

Python ist auf vielen Plattformen verfügbar. Sie können also Python direkt ab der Website herunterladen und installieren. Das führt womöglich aber dazu, dass sie sehr oft sich um die Aktualisierung kümmern müssen. Noch schlimmer wird es, wenn Sie verschiedene Versionen nebeneinander betreiben müssen. Da kriegt man bald ein Durcheinander.

Alternativ lässt sich Python in einer virtuellen Maschine installieren. Wir empfehlen das Windows-Subsystem für Linux (WSL), wobei wir für WSL2 plädieren. Eine Übersicht finden Sie auf [WikiPedia](https://de.wikipedia.org/wiki/Windows-Subsystem_f%C3%BCr_Linux). Sollten Sie nicht Windows verwenden, ist diese Problematik für Sie hinfällig.

Jetzt bietet Python noch eine ganz schöne Funktion an: Virtuelle Umgebungen bzw. virtual Environment (kurz venv). Sie können damit in einem Verzeichnis eine separate Python-Umgebung schaffen. Wechseln Sie mit `cd` ins Verzeichnis, werden Pakete und Versionen geladen, die Sie für das vorliegende Projekt benötigen. Verlassen Sie das Verzeichnis wieder, wird diese zusätzliche Software wieder entfernt. Ihr System bleibt also "sauber". Auch unsere IDE *Visual Studio Code* kommt damit ausgezeichnet klar.

## Installationsvorgang

Im Folgenden wird von einer Ubuntu-Konsole im WSL ausgegangen. Womöglich müssen Sie das eine oder andere adaptieren.

1. Installieren Sie `python3` inkl. der `pip3` und `venv` packages
```sh
$ sudo apt install python3-pip python3-venv
```
1. Erstellen Sie ein neues Arbeitsverzeichnis
```sh
$ mkdir my-first-project && cd my-first-project
```
1. Starten Sie VS Code und öffnen das neue Verzeichnis darin
```sh
$ code .
```
1. Erstellen Sie ein *virtuelles Environment* 
```sh
$ python3 -m venv .venv
```
1. Laden Sie Ihr *Virtuelles Environment* 
Diesen Schritt müssen Sie jeweils wiederholen, wann immer Sie mit ansible arbeiten.
```sh
$ source .venv/bin/activate
```
1. pip aktualisieren
```sh
$ python -m pip install --upgrade pip
```
1. Erstellen Sie die `requirements.txt`.
```sh
$ echo "ansible" > requirements.txt
```
Die einfachste Version enthält nur `ansible`. Falls die Module weitere Python Packages benötigen, müssen Sie diese natürlich ergänzen (zum Beispiel für aws, Proxmox, Molecule, usw.)
1. Python-Requirements installieren
```sh
$ pip install -r requirements.txt
```

!!! success "Lernerwartung"
    Sie haben hier ...
    
    - sich Python installiert
    - das Paket venv in Betrieb genommen
    - ansible installiert

    Dabei haben Sie sich gemerkt, welche Schritte Sie wofür benötigen und in welchem Fall welche Schritte wiederholen müssen. Python ist eine wichtige Umgebung im Umfeld des Cloud-Computings.