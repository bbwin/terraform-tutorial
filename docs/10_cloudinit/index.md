# Einstieg ins MaaS

!!! abstract "Voraussetzung"
    - Login an der BBW
    - Zugriff auf [https://maas.bbw-it.ch](https://maas.bbw-it.ch)

## MaaS @ BBW

Das MaaS der BBW ist eine lokale Cloud-Umgebung, auf welcher Sie VMs erstellen können. Steigen Sie über [https://maas.bbw-it.ch](https://maas.bbw-it.ch) ein und finden Sie alles Notwendige unter [help](https://maas.bbw-it.ch/help.php).

!!! task "Aufgabe"
    Erstellen Sie eine VM, auf welche Sie per SSH Zugriff kriegen. Der in der Hilfe verlinkte Film half Ihren Vorgängern.

!!! success "Lernerwartung"
    Sie haben eine WireGuard-VPN ins MaaS eingerichtet.

    Sie können nun ...

    - auf dem MaaS eine VM erstellen
    - auf eigene VMs per SSH zugreifen

!!! info "Neues Werkzeug"
    Damit haben Sie ein neues Werkzeug zur Verfügung gekriegt, welches Sie auch in anderen Modulen einsetzen werden.