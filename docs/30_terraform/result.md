# Resultat

1. Erstellen Sie einen gitlab-Account, falls noch nicht geschehen.
1. Erstellen Sie ein Projekt.
1. Laden Sie Ihre Lehrperson als Gast ein.
1. Ordnen Sie Ihre Ergebnisse aus diesem Kurs und checken Sie das Ergebnis ein.
1. Erstellen Sie eine README.md und fassen Sie alles zusammen.