terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "us-west-2"
}

# This object holds all the availability zones in our region
data "aws_availability_zones" "available" {
  state = "available"
}

# This object holds the latest Ubuntu AMI
data "aws_ami" "ubuntu_22_04" {
  # Only select the most recent image
  most_recent = true

  # Filter images with ubuntu-jammy-22.04-amd64-server in name
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  # We need images with virtualization-type "hvm"
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  # Canonical's AWS account ID
  owners = ["099720109477"]
}

# Create a virtual private cloud (VPC)
# https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  # Enable DNS hostnames
  enable_dns_hostnames = true
}

# To access the internet from a VPC, we need to create a internet gateway
# and attach it to the VPC
# https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

# Create private subnets to host our RDS database
# Only the public subnet will be able to communicate with this subnet
resource "aws_subnet" "private" {
  # RDS requires 2 subnets for a database
  count = 2

  # Attach it to our VPC
  vpc_id = aws_vpc.main.id

  # Get the IPv4 CIDR block for the VPC, increase the prefix by 8 (16->24)
  # Set the additional bits to a value 10 + the index (10 and 11 in this example)
  cidr_block = cidrsubnet(aws_vpc.main.cidr_block, 8, 10 + count.index)

  # We grab the availabilty zone from the data object
  availability_zone = data.aws_availability_zones.available.names[count.index]

  # Tag the subnet with "private" and the name of the availability zone
  tags = {
    Name = "private_${data.aws_availability_zones.available.names[count.index]}"
  }
}

# Create public subnet for our webservice
resource "aws_subnet" "public" {
  # Attach it to our VPC
  vpc_id = aws_vpc.main.id

  # Set the subnet
  cidr_block = "10.0.0.0/24"

  # Instances in this subnet get a public IP
  map_public_ip_on_launch = true
}

# Create a route table for the public subnet
resource "aws_route_table" "public" {
  # Attach it to our VPC
  vpc_id = aws_vpc.main.id

  # For the public route table, we need access to the internet
  # Adding a route with 0.0.0.0/0 as as destination and target
  # the Internet Gateway
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}

# Add public subnets to public route table
resource "aws_route_table_association" "public" {
  route_table_id = aws_route_table.public.id

  subnet_id = aws_subnet.public.id
}

# Create a route table for the private subnet
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  # This is a private subnet with no internet access
  # We don't add any routes
}

# Add private subnets to private route table
resource "aws_route_table_association" "private" {
  # We created 2 private subnets, so we read the count variable from there
  count = length(aws_subnet.private)

  route_table_id = aws_route_table.private.id

  # Read the ids from the aws_subnet object
  subnet_id = aws_subnet.private[count.index].id
}

# Create a security group for our EC2 instance (Firewall)
resource "aws_security_group" "ec2" {
  name        = "ec2-allow-ssh-http"
  description = "Security group for the EC2 instance"
  vpc_id      = aws_vpc.main.id

  # Allow SSH from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow HTTP from anywhere
  ingress {
    from_port   = 80
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound internet traffic
  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # If we modify this security group, terraform normaly destroys the 
  # security group and creates a new one. This leads to a recreation of
  # our EC2 instance. With create_before_destroy terraform first creates 
  # a new security group before it destroys the old one, avoiding a recreate
  # of the EC2 instance
  lifecycle {
    create_before_destroy = true
  }
}

# Create a security group for our database
resource "aws_security_group" "rds" {
  name        = "rds-allow-sql"
  description = "Security group for the RDS instance"
  vpc_id      = aws_vpc.main.id

  # RDS database should not be accessible from the internet
  # We will therefore not add any ingress or egress rules for
  # outside traffic

  # Only our EC2 instance should be able to communicate with RDS
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.ec2.id]
  }

  # Same principle for RDS
  lifecycle {
    create_before_destroy = true
  }

}

# Create a RDS database with MariaDB
resource "aws_db_instance" "mariadb" {
  identifier             = "jokesdb"
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mariadb"
  engine_version         = "10.6.14"
  instance_class         = "db.t2.micro"
  db_name                = "jokesdb"
  username               = "admin"
  password               = "mypassword"
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds.id]
  db_subnet_group_name   = aws_db_subnet_group.private.name
}

resource "aws_db_subnet_group" "private" {
  name = "db_subnet_group"

  # RDS requires 2 or more subnets, so we loop through our private subnets
  subnet_ids = [for subnet in aws_subnet.private : subnet.id]
}

resource "aws_instance" "ec2_instance" {
  # Get the latest Ubuntu AMI from the data object
  ami           = data.aws_ami.ubuntu_22_04.id
  instance_type = "t2.micro"

  # This instance should be available from the internet
  # Place it in the public subnet
  subnet_id = aws_subnet.public.id

  # Attach the security groups (firewall) to allow ingress and egress
  vpc_security_group_ids = [aws_security_group.ec2.id]

  # Add SSH public key to EC2 instance so we can connect with SSH
  key_name = aws_key_pair.rsa.key_name

  # We fetch the AMI from the latest Ubuntu image dynamically
  # We want avoid recreating the instance in case the AMI changes
  lifecycle {
    ignore_changes = [ami]
  }
}

# Read id_rsa from local workstation
resource "aws_key_pair" "rsa" {
  key_name   = "ssh_key"
  public_key = file("~/.ssh/id_rsa.pub")
}

output "web_public_ip" {
  description = "The public IP address of the web server"

  value = aws_instance.ec2_instance.public_ip
}

output "database_endpoint" {
  description = "The endpoint of the database"
  value       = aws_db_instance.mariadb.address
}

output "database_port" {
  description = "The port of the database"
  value       = aws_db_instance.mariadb.port
}