# Remote-Verwaltung von Systemen

Grundsätzlich ist eine Command Line Interface ein tolles Werkzeug. Man kann mit kaum Ressourcen (Server- wie Clientseitig) ein unglaublich flexible Zugang ermöglichen. Und man kann sogar copy/pasten, was sehr oft die Haut rettet!

Lokal lernten Sie schon verschiedene CLIs kennen. Wenn Sie aus der Ferne ein System benutzen, verwenden wir fast immer SSH. Das ist eine Weiterentwicklung von telnet bzw. rsh.

## Was ist SSH?

!!! question "Auftrag 1: Was ist SSH?"
    Bearbeiten Sie den Artikel [Was ist SSH? | heise online](https://www.heise.de/tipps-tricks/Was-ist-SSH-7313396.html).

    <details><summary>Mögliche Methode "Gruppenlesen" zu dritt</summary>

    - Person **a** liest 1-2 Sätze laut.
    - Person **b** gibt den Inhalt in eigenen Worten wieder.
    - Person **c** kontrolliert und interveniert gegebenenfalls.
    - Wechsel
    
    Was sind Ihre Erkenntnisse aus dem Artikel?  
    Besprechen Sie die Unterschiede und Gemeinsamkeiten von Passwörter und SSH Schlüsselpaaren.
    </details>

## Funktionsweise SSH

Bevor wir uns in die Automatisierung und Private Cloud wagen, müssen wir vorerst die Funktionsweise von SSH verstehen. Die zugrundeliegende Theorie mit asymmetrischer Verschlüsselung behandelten Sie im Modul 114 "Codierungs-, Kompressions- und Verschlüsselungsverfahren einsetzen".

Bei jedem Aufruf von `ssh user@server` authentifizieren sich Client und Server gegenseitig. Nach dem Verbindungsaufbau sendet der Server ein Zertifikat zur Verifikation, ob es sich wirklich um den richtigen Server handelt. Der Fingerprint des Zertifikats wird Ihnen beim ersten Verbindungsaufbau jeweils zur Bestätigung angezeigt und Sie müssen die Verbindung zuerst mit `yes` erlauben.

```sh
$ ssh user@server                                       
The authenticity of host 'server (95.217.182.89)' can't be established.
ED25519 key fingerprint is SHA256:F8f9J9gLNgDgbZJ2rsdpFAlq23KrWIwuevhg9W8Ap2E.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

Weiter werden nötige Parameter zur Verschlüsselung der Verbindung gesendet, bevor der Client sich einloggen kann.

![image-20230918120445501](assets/verbindungsaufbau.png)

Sie haben höchstwahrscheinlich in den bisherigen Modulen, in denen Sie SSH verwendet haben, hauptsächlich mit Passwörtern gearbeitet:

```sh
$ ssh user@server
user@server's password: 
```

Der erste Schritt für mehr Sicherheit und Komfort liegt im Umstieg von Passwörtern auf Schlüsselpaare. Wenn Sie die Anmeldung mit Passwort erlauben (in den gängigen Cloud-Umgebungen sind Passwörter übrigens bereits standardmässig deaktiviert), geben Sie potenziellen Eindringlingen die Chance, Benutzernamen und Passwörter durchzuprobieren. Funktioniert die Anmeldung am SSH-Server dagegen ausschliesslich mit gültigem Schlüssel, prallen alle Anfragen ab, die nicht den passenden Schlüssel im Gepäck haben.

Bei der Anmeldung mit Schlüsseln (Public-Key-Authentifizierung) handelt es sich um ein Verfahren der asymmetrischen Kryptografie. Es braucht ein Schlüsselpaar, das aus einem privaten und einem öffentlichen Schlüssel besteht. Der öffentliche Schlüssel liegt auf dem Zielsystem / dem SSH-Server. Der private Schlüssel verbleibt auf Ihrem lokalen System / dem SSH-Client.

!!! danger "Privater Schlüssel"
	Passen Sie gut auf Ihren privaten Schlüssel auf. Teilen Sie ihn niemals mit anderen.

Mit dem Befehl `ssh-keygen` können Sie auf Ihrem lokalen System (benutzen Sie eine WSL Kommandozeile, Powershell oder verbinden Sie sich per `multipass shell` in ein Ubuntu) ein neues Schlüsselpaar erzeugen:

```sh
$ ssh-keygen                        
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/user/.ssh/id_rsa
Your public key has been saved in /home/user/.ssh/id_rsa.pub
```

Standardmässig erstellt `ssh-keygen` in Ihrem Home-Verzeichnis unter `.ssh` die Datei `id_rsa` mit Ihrem privaten Schlüssel, in einer weiteren mit der zusätzlichen Endung `.pub` der dazugehörige öffentliche.

## SSH Schlüsselpaar

!!! question "Auftrag 2: SSH Schlüsselpaar generieren"
    Erstellen Sie sich gemäss Anleitung ein RSA Schlüsselpaar. Lassen Sie sich den *öffentlichen* Schlüssel mittels `cat` oder `nano` anzeigen und speichern Sie diesen an einem Ort, an dem Sie einfach darauf zugreifen können. Sie werden Ihn für die weiteren Aufgaben noch vielfach verwenden.

!!! info "PuTTY vs. OpenSSH Schlüsselformat"
    OpenSSH stellt den Standard des Formats, wie Schlüssel abgespeichert werden. PuTTY hält sich leider nicht daran und verwendet ein eigenes Format. Die beiden Schlüssel sind aber zueinander kompatibel, müssen aber womöglich umformatiert werden. Das bedeutet, falls Sie PuTTY verwenden wollen, müssen Sie z.B. den Private Key im PuTTY-Format bereit halten, weil aber auf dem Server OpenSSH installiert ist, benötigen Sie den dazu passenden Public Key im OpenSSH-Format. *WinSCP* basiert ebenfalls auf PuTTY und bietet einen automatischen Umwandler. Wir empfehlen deshalb auf PuTTY zu verzichten.

!!! success "Lernerwartung"
    In diesem Kapitel haben Sie die Funktionsweise von SSH kennen gelernt und ein persönliches RSA Schlüsselpaar für die weiteren Aufgaben erstellt.
