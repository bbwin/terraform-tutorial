# Terraform - Erste Virtuelle Instanz

![Terraform](assets/terraform.svg)

!!! abstract "Voraussetzung"
    - aws Learner Lab ([Video](https://youtu.be/T9RV-s25dGo))

## main.tf vorbereiten
Als erstes bauen wir ein einfaches Terraform main.tf, um eine ec2-Instanz in Betrieb zu nehmen:

```hcl
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "app_server" {
  ami           = "ami-03f65b8614a860c29"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
```

Mehrheitlich ist das main.tf durchaus selbsterklärend. Der Einstiegspunkt ist der Block Terraform, in welchem gerade einmal die providers definiert werden. Wir haben den Provider `aws` im Einsatz, wobei wir die Quelle (Terraform besorgt das entsprechende Modul dann selbständig) ab einer gewissen Version angeben. Terraform selbst erwarten wir ab der Version 1.2.0.

Im zweiten Abschnitt bestimmen wir einen Parameter für den Provider aws, nämlich in welcher Region wir arbeiten wollen.

Jetzt geht's um die eigentliche Ressource, die wir nutzen wollen. Wir nutzen die Ressource `aws_instance` und nennen diese für uns app_server. Dabei soll das Image verwendet werden, welches bei ami angegeben wird. Zudem bestimmen wir den Instanzen-Typ, sprich die Grösse der Maschine. Wir können der Ressource noch Tags mitgeben, was für weitere Verarbeitungen nützlich sein kann.

Woher aber kennen wir die ami-ID? Am Anfang geht man einfach über das WebGUI bei aws schauen. Wenn man nämlich eine neue Instanz baut, wählt man das Image aus und da steht diese ID. Aber da wir uns vom WebGUI lösen wollen, suchen wir einen anderen Weg. Fügen Sie folgenden Block vor die Ressource (eigentlich ist die Reihenfolge völlig egal) ein:

```hcl
data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
```

und definieren Sie dann in der Ressource `ami = data.aws_ami.ubuntu.id`. Jetzt sucht sich Terraform selbständig das neueste ubuntu-jammy-22.04-amd64-Image und setzt dieses ein. In produktiven Umgebungen will man sowas selten (alles muss sauber versioniert sein), aber in Testumgebungen kann das ausserordentlich nützlich sein. So bleibt man immer auf dem neuesten Stand!

Sie haben damit die zwei wichtigesten Elemente von Terraform kennengelernt: `ressource` sind Komponenten, welche von Terraform erstellt und kontrolliert werden. Mit `data` ermöglichen wir Terraform, auch externe Komponenten auszulesen und für unsere Ressourcen zu verwenden.

## Zugriff zu aws ermöglichen

Wir benötigen noch ein paar Sachen, damit Terraform unsere IaC verarbeiten kann. Als erstes benötigen wir den Zugang ins aws. In der Realität haben Sie dazu einmalig credentials, die Sie immer wieder gebrauchen. Mit dem Learner Lab ist das leider etwas mühsamer, dafür kostenlos:

Sie gehen auf die [aws Lernplattform](https://awsacademy.instructure.com) und gehen da zum Modul, welches das Learner Lab beinhaltet. Dieses wurde hoffentlich von Ihrer Lehrperson schon frei gegeben. Wenn Sie das Lab gestartet haben, klicken Sie neben `Start Lab` den Knopf `AWS Details` und neben AWS CLI den `Show`-Knopf. Die jetzt angezeigten credentials copy/pasten Sie wie beschrieben in Ihrer Workstation ins `~/.aws/credentials`. Damit haben Sie gerade Ihrer aws cli und auch Terraform Zugang zu Ihrem aws-Account gegeben.

Wenn die AWS-Umgebung runter fährt (und das macht sie spätestens nach 4 h), müssen Sie diese Zugangsdaten neu besorgen.

## main.tf ausführen

Terraform ist sehr mächtig: Die Ausführung führt dazu, dass Infrastrukturen (und diese können sehr umfangreich werden), komplett aufgebaut, abgebaut oder auch umgebaut werden. Kleine Fehler können hier riesige Schäden anrichten. Zum Glück spielen wir zur Zeit nur ein wenig damit herum. Aber weil das so ist, sind Vier-Augen-Prinzip, dry-runs und sauber durchgeführte Pipelines mit Fehlererkennung ausserordentlich wichtig. Deshalb gewöhnen wir uns auch eine Verhaltensweise an, die als *Best practice* bekannt ist.

### init
Zur Initialisierung wird `terraform init` benötigt. Dies führt das erste Mal auch dazu, dass entsprechende Module geladen werden und auch zusätzliche Dateien zum Betrieb eingerichtet werden. Konkret ist dies das `terraform.tfstate` und `terraform.tfstate.backup`. Diese Datei ist für Terraform derart wichtig, dass gerade auch ein backup davon erstellt wird.

### fmt
Mittels `terraform fmt` wird das File sauber formatiert. Um die Lesbarkeit und Codequalität hoch zu halten, hilft Ihnen das Tool Ihr Code sauber formatiert zu halten. Wenn Sie noch den Parameter `-diff` anhängen, zeigt Ihnen Terraform auch noch, welche Anpassungen vorgenommen wurden.

### plan
Als nächstes wird `terraform plan -out=job2do` angewandt. Dabei wird sozusagen ein dry-run durchgeführt, also eine Ausführung ohne Ausführung. Der Output ist enorm wichtig, damit wir nicht aus Versehen blödsinn anstellen. Es wird nun nämlich geprüft, was Terraform an der Infrastruktur ändern muss, damit der neue SOLL-Zustand erreicht wird. Das muss uns unbedingt interessieren.

Das Mindeste, was kontrolliert werden sollte, sind die Zahlen in der letzten Zeile: Wieviele Ressourcen werden hinzugefügt, verändert bzw. entfernt. In Testumgebungen reicht es womöglich, hier einen Blick darauf zu werfen, kurz zu überlegen und dann das Ergebnis als OK oder Nicht-OK zu taxieren.

### apply
Falls das Ergebnis OK ist, führen wir `terraform apply job2do` aus. Jetzt wird nochmals die gleiche Anzeige wie bei `plan` dargestellt, nur wird jetzt auch der eigentliche Job ausgeführt.

### destroy
Mit `terraform destroy` entfernen Sie die gerade erstellte Infrastruktur wieder.

!!! task "Diskussion mit Tandem"
    Nachdem Sie nun obiges ausprobiert haben und heraus fanden, dass nun eine EC2-Instanz erstellt wurde und Sie sie auch wieder entfernen konnten, suchen Sie ein Tandem, um folgende Fragen zu klären:

    - Wieso ist der Parameter `-out` wichtig? Welche Bedingungen müssten vorhanden sein, dass was schief laufen könnte? Was könnte schief laufen, würde man darauf verzichten?

    - Wo wird Terraform ausgeführt, falls Sie als Firma / Abteilung Infrastrukturen betreiben?

    - Wie greifen Sie nun auf die gestartete ec2-Instanz zu?

!!! success "Lernerwartung"

    Sie haben in diesem Kapitel ...

    - main.tf kennen gelernt
    - mittels `terraform` im Learner's Lab Instanzen hoch gefahren
    - `terraform` bedient
    - das Verfahren im Handling mit `terraform` angewandt

    Diese Kompetenzen werden Sie ab sofort einsetzen.
