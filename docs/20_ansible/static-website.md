# Statische Website mit Ansible

!!! abstract "Voraussetzungen"
    Sie errichteten mit [Cloudinit die statische Website](../10_cloudinit/static-website.md).

In dieser Übung geht es darum, die mit CloudInit statisch erstellte Website mit Ansible umzusetzen. Das bietet den Vorteil, dass wir auch Änderungen vornehmen können, ohne gerade die ganze VM neu zu bauen.

!!! task "Vorbereitung"
    Wir benötigen zuerst eine VM (auf dem MaaS), auf welcher wir die Website in Betrieb nehmen möchten. Diese VM muss von Ansible per SSH erreichbar sein, weshalb wir einen Applikations-User erstellen. Ein fertiges Script steht im [CloudInit ReadTheDocs](https://cloudinit.readthedocs.io/en/latest/reference/examples.html#configure-instance-to-be-managed-by-ansible) zur Verfügung. Es wird empfohlen, eigene SSH-Keys zu verwenden.

    Ansible benötigt zudem ein Inventar über seine zu verwaltenden Rechner. Erstellen Sie gemäss vorheriger Anleitung die `hosts`-Datei und auch die `ansible.cfg`.

## Ansible Playbook
Spannender wird es nun mit dem Playbook.

```yaml
---
- name: Update webserver
  hosts: web
  remote_user: ansible

  tasks:
  - name: Latest version of nginx
    ansible.builtin.apt:
      name: nginx
      state: latest

  - name: Write the index.html
    ansible.builtin.copy:
      src: index.html
      dest: /var/www/index.html
      mode: '0644'
```

Zuerst wird ein Playbook "Update webserver" erstellt, welches auf die Hosts der Gruppe web angewandt wird. Es wird ansible als Login verwendet. Der Rest ist wohl selbsterklärend, insbesondere wenn Sie ja die Schritte vom CloudInit kennen.

!!! task "Lernen ist aktives Handeln!"
    Vom Lesen wird man nicht gescheit. Aber wenn ich lese und umsetze, beginnt es zu arbeiten. Und wenn ich meine Erkenntnisse dann ordne, niederschreibe und nochmals überarbeite, dann kann ich Gelesenes aufnehmen.

!!! task "Einlesen"
    Jetzt gilt es, sich mit der [Doku von Ansible](https://docs.ansible.com/ansible/latest) anzufreunden. Wo lassen sich die Informationen finden? Versuchen Sie folgende Aufgaben anhand der Dokumentation zu lösen:

    - Welche Alternativen gäbe es statt `state: latest`? Ist das überhaupt, was wir machen wollen?
    - Liesse sich das index.html auch direkt ins Playbook aufnehmen, anstelle einer Kopie einer zusätzlichen Datei?
    - Es gibt die Collection `ansible.builtin.template`, worin unterscheiden sie sich von `ansible.builtin.copy`?
    - Mir gefällt die Cloudinits Möglichkeit von `gzip/base64`. Wie wäre dies in Ansible umsetzbar?
    - Natürlich möchten wir noch die Aufgabe mit zusätzlichem Bild erfüllen.
    - Wozu sind diese 3 Bindestriche zu Beginn?

!!! success "Lernerwartung"
    Sie haben ein kleines Ansible Projekt aufgebaut und umgesetzt.